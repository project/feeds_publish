<?php

/**
 * @file
 * Admin callbacks for feeds publishing.
 */

/**
 * Page callback for admin/structure/feeds/feeds-publish.
 */
function feeds_publish_config() {
  $form = array();

  $form['feeds_publish_start_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Query start time'),
    '#description' => t('Time in hours for how long ago feed data should be checked for unpublished nodes. This normally should be around 5 hours depending on how often your cron runs.'),
    '#default_value' => variable_get('feeds_publish_start_time', 5),
    '#size' => 5,
    '#element_validate' => array('element_validate_integer'),
  );

  $form['feeds_publish_end_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Query end time'),
    '#description' => t('Time in hours for how long feed node should be left as unpublished before automatically publishing. This number should be less than query start time.'),
    '#default_value' => variable_get('feeds_publish_end_time', 1),
    '#size' => 5,
    '#element_validate' => array('element_validate_integer'),
  );

  return system_settings_form($form);
}
