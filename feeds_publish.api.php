<?php

/**
 * @file
 * API Hook definitions for feeds_publish module.
 */

/**
 * Checks if item should be selected for automatic publishing or not prior to
 * loading the full node object.
 *
 * @param object $item
 *   Object with the following properties:
 *   - nid (integer) Node ID.
 *   - title (string) Title of node object.
 *   - status (boolean) 1 for published, 0 for unpublished.
 *   - feed_nid (integer) Parent Feed ID for item.
 */
function hook_feeds_publish_pre_condition_check_alter($item) {
  // Custom processing here.

  // If item should not pass validation set $item->fail.
  $item->failed = TRUE;
}

/**
 * Checks if item should be selected for automatic publishing or not after
 * loading the full node object.
 *
 * @param object $item
 *   Object with the following properties:
 *   - nid (integer) Node ID.
 *   - title (string) Title of node object.
 *   - status (boolean) 1 for published, 0 for unpublished.
 *   - feed_nid (integer) Parent Feed ID for item.
 * @param object $node
 *   Drupal node object.
 */
function hook_feeds_publish_post_condition_check_alter($item, $node) {
  // Custom processing here.

  // If item should not pass validation set $item->fail.
  $item->failed = TRUE;
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Specify your configuration options to feeds publish for your custom
 * conditions here. Set the $form array with the elements you want to add. Your
 * module changes should be wrapped in a fieldset. See example below:
 */
function hook_form_feeds_publish_config_alter(&$form, &$form_state) {
  $form['your_module'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => t('Your Module'),
  );

  $form['your_module']['your_module_settings_var'] = array(
    '#type' => 'textfield',
    '#title' => t('Your setting title'),
    '#description' => t('Your setting description'),
    '#default_value' => variable_get('your_module_settings_var', 'default_value'),
  );
}
