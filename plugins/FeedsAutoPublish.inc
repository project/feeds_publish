<?php

/**
 * @file
 * Class definition for autopublishing feeds.
 */

/**
 * Container class to autopublish feeds.
 */
class FeedsAutoPublish {

  /**
   * Gets all unpublished nodes based on criteria.
   *
   * @return array of objects with the following values:
   *   - nid (integer) Node ID.
   *   - title (string) Title of node object.
   *   - status (boolean) 1 for published, 0 for unpublished.
   *   - feed_nid (integer) Parent Feed ID for item.
   */
  public function getUnpublishedNodes() {
    $query = db_select('node', 'n');
    $query->leftJoin('feeds_publish_map', 'm', 'n.nid = m.nid');
    $query->innerJoin('feeds_item', 'f', 'n.nid = f.entity_id');
    $query->fields('n', array('nid', 'title', 'status'));
    $query->fields('f', array('feed_nid'));
    $query->fields('m', array('processing'));
    $query->condition('n.status', 0, '=');

    $current_time = time();

    // Convert constants into seconds.
    $query->condition('n.created', $current_time-FEEDS_PUBLISH_END_TIME*60*60, '<=');
    $query->condition('n.created', $current_time-FEEDS_PUBLISH_START_TIME*60*60, '>=');

    $result = $query->execute()->fetchAll();
    return is_array($result) ? $result : array();
  }

  /**
   * Builds array mapping of feeds nids and their source.
   *
   * @return array
   *   Array keyed by feed node ID and values of feed parent ID.
   */
  public function getFeedSources() {
    $results = db_select('feeds_source', 'f')
      ->fields('f', array('feed_nid', 'id'))
      ->execute()
      ->fetchAll();

    $return = array();
    foreach ($results as $result) {
      if (!isset($result->feed_nid) && !isset($result->id)) {
        continue;
      }
      $return[$result->feed_nid] = $result->id;
    }
    return $return;
  }

  /**
   * Queue population method.
   *
   * Populates queue with unpublished nodes to be processed.
   */
  public function processCron() {
    $items = $this->getUnpublishedNodes();
    $nodes = $this->checkConditions($items);
    $this->addQueue($nodes);
  }

  /**
   * Check for conditions.
   *
   * @param array $data
   *   Array of objects generated from getUnpublishedNodes().
   *
   * @return array
   *   Array of full loaded node objects.
   */
  public function checkConditions($data) {
    $return = array();
    foreach ($data as $item) {
      // When item is already added to queue the item is skipped.
      // Queue addition determined by processing column in feeds_publish_map.
      if (isset($item->processing) && (int) $item->processing == 1) {
        continue;
      }
      $feeds_map = $this->getFeedSources();
      $item->feed_parent = isset($feeds_map[$item->feed_nid]) ? $feeds_map[$item->feed_nid] : '';

      // Invoke a pre condition hook to see if node shuold pass.
      drupal_alter('feeds_publish_pre_condition_check', $item);
      if (isset($item->failed)) {
        // Item failed so set mapping table and keep looping.
        $this->updateMapping($item, array(
          'valid' => 0,
          'processing' => 0,
        ));
        continue;
      }

      // Make sure a real node is found.
      $node = node_load($item->nid);
      if (!isset($node->nid)) {
        continue;
      }

      // Allow other modules to set item to failure.
      drupal_alter('feeds_publish_post_condition_check', $item, $node);
      if (isset($item->failed)) {
        // Item failed so set mapping table and keep looping.
        $this->updateMapping($item, array(
          'valid' => 0,
          'processing' => 0,
        ));
        continue;
      }

      $return[] = $node;

      // Cleanup node object so the same one doesn't get used again.
      unset($node);
    }
    return $return;
  }

  /**
   * Adds nodes to queue for publish processing.
   *
   * @param array $nodes
   *   Array of fully loaded node objects.
   */
  public function addQueue($nodes) {
    foreach ($nodes as $node) {
      if (!isset($node->nid)) {
        watchdog('feed_publish: addQueue', 'Node value not found to update mappings', array(), WATCHDOG_NOTICE);
        continue;
      }
      $queue = DrupalQueue::get('FeedsAutoPublish');
      $queue->createQueue();
      $queue->createItem($node);

      // Update the mapping table that feed is set to be processed.
      $this->updateMapping($node, array(
        'processing' => 1,
        'valid' => 1,
      ));
    }
  }

  /**
   * Updates mapping table with node autopublishing statuses.
   *
   * - "status" is published/unpublished boolean from node object.
   * - "nid" is node ID.
   * - "processing" dictates that node has been added to queue for processing.
   * - "valid" says whether node is valid for processing or not.
   *
   * @param object $node
   *   Drupal node object.
   * @param array $values
   *   Associative array of processing values:
   *   - processing (boolean): If node has been added to queue for processing.
   *   - valid (boolean): If node is valid for processing.
   */
  public function updateMapping($node, $values) {
    if (!isset($node->nid)) {
      watchdog('feed_publish: updateMapping', 'Node value not found to update mappings', array(), WATCHDOG_NOTICE);
      return FALSE;
    }
    $result = db_merge('feeds_publish_map')
      ->key(array('nid' => $node->nid))
      ->fields(array(
        'nid' => $node->nid,
        'status' => $node->status,
        'processing' => $values['processing'],
        'valid' => $values['valid'],
      ))
      ->execute();
  }

  /**
   * Publishes nodes to site.
   *
   * @param object $node
   *   Drupal node object.
   *
   * @return boolean
   *   TRUE on success, FALSE on failure.
   */
  public function processNode($node) {
    if (!isset($node->nid)) {
      return FALSE;
    }
    node_publish_action($node);
    node_save($node);
    $this->updateMapping($node, array(
      'processing' => 0,
      'valid' => 1,
    ));

    return TRUE;
  }
}
